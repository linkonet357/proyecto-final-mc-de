#pragma once
#include <iostream>
#include <conio.h>
#include<fstream>
#include<sstream>
#include "CItem.h"

using namespace std;

class CEnemy{
	public:
		
	string strNombre, strNombreAtaque;
	int iHP, iNivelEnemigo = 1, iExp, iID, iAtk, iDef, iOro = 0;
	
	vector <CItem> inventario;

	CEnemy();
	~CEnemy();
	void Combate(int iID, int iNivel); //La funcion de Combate
	
	void Drop(CItem); // La funcion que envia el objeto del enemigo al inventario
	
};
