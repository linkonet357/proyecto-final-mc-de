#ifndef ITEM_H
#define ITEM_H
#include <iostream>
#include <vector>

using namespace std;

class CItem{ // La clase base con la que manejaremos todos los objetos
	public:
	int iObjetos = 0; // nose que hace esta variable Diego
	
	virtual void descripcion() { //Funcion de descripccion que todas Heredaran y modificaran
		cout << "Soy un objeto"; cout << "Tengo un precio de : "<<  endl; 
	}

};

class CWeapon: public CItem{ //La clase base de objetos equipables
	public:
		
};

class CEspadaHierro : public CWeapon{ //Clase de objeto equipable
	public:
		void descripcion() {
		cout << "Espada de Hiero: "; cout << "Es una espada de Hierro. "; cout << "Costo de 25G" << endl;
	}
};

class CCascoHierro : public CWeapon{ //Clase de objeto equipable
	public:
	void descripcion() {
	cout <<"Casco de Hierro: "; cout << "Es un casco de hierro. "; cout <<"Costo de 40g" << endl;
	}
};


class CConsumible: public CItem{ //La clase base de objetos consumibles
	public:
		
};

class CPocion : public CConsumible{ //Clase de objeto Consumible
	public:
		
	void descripcion() {
	cout << "Pocion: "; cout << "Es una pocion que recupera 50 VP. "; cout <<"Costo de 25G" << endl;
	}
};

class CMP : public CConsumible{ //Clase de objeto Consumible
	public: 
	
	void descripcion() {
	cout << "Pocion de MP: "; cout << "Es una pocion que recupera 50 MP. "; cout<< "Costo de 25G" << endl;
	}
	
};

class CAttackItem: public CItem { //La clase base de objetos consumibles con efecto
	public:
		
};

class CBomba : public CAttackItem{ //Clase de objeto Consumible con efecto
	void descripcion() {
	cout << "Bomba: "; cout << "Es una Bomba explosiva, se usa en batalla. "; cout <<"Costo de 50g" << endl;
	}
};

void inventario();

#endif // !CItem_H
