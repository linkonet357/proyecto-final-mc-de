#include "CPlayer.h"

CPlayer :: CPlayer(){
	iHP  = 20; //Vida del Jugador
	iPNivel= 1; // Nivel del Jugador
	iExp = 1; // Experiencia actual del Jugador
	iAtk = 5; // Cantidad de Ataque del Jugador
	iDef = 10; // Cantidad de Defenza del Jugador

	iOro = 0; // Cantidad de Oro del Jugador
}

void CPlayer::stats(){ //Funcion que imprime las estadisticas actuales del jugador
	system("cls");
	
	cout << "Pv: " << iHP << endl;
	cout << "Lv: " << iPNivel << endl; 
	cout << "Exp: " << iExp << endl;
	cout << "Atk: " << iAtk << endl;
	cout << "Def: " << iDef << endl;
	
	cout <<	"Cantidad de Oro: " << iOro << endl;
}
