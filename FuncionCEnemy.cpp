#include "CEnemy.h"


CEnemy::CEnemy()
{
	iAtk = 0; //Ataque del enemigo
	iDef = 0; // Defenza del enemigo
	iExp = 0; //Experiencia que da el enemigo al derrotarlo
	iHP = 0; //Vida del Enemigo
	iID = 0; //Identificador del enemigo
	iNivelEnemigo = 0; // Nivel del Enemigo
	iOro = 0; 
}

CEnemy::~CEnemy() { // Para cuando muera el Enemigo



}

void CEnemy::Combate(int iID, int iNivel) //Funcion para que spawnee un enemigo
{
	ifstream fileSaveReader("Enemy.txt");
	string sFileLine;
	int iCont = 0, iConta = 0;
	

	if (fileSaveReader.is_open())
	{
		while (iConta <= iID) { // Este while, funciona para seleccionar un Enemigo

			getline(fileSaveReader, sFileLine, '\n');
			iConta ++;
		}
		while (getline(fileSaveReader, sFileLine, '@')) // Este while, le pone las stats al enemigo
		{
			switch (iCont)
			{
			case 0: strNombre = sFileLine;
				break;
			case 1: iAtk = stoi(sFileLine);
				break;
			case 2: iDef = stoi(sFileLine);
				break;
			case 3: iHP = stoi(sFileLine);
				break;
			case 4: iOro = stoi(sFileLine);
				break;
			}
			++iCont;
		}
		fileSaveReader.close();
	}
	cout << "Acaba de aparecer un  " << strNombre << ", es nivel " << iNivel << ", y estar cargando " << iOro << " de oro. Lleva un  " << endl;
}



