 #include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <sstream>	
#include <vector>
#include <conio.h>

#include "CPlayer.h"
#include "CEnemy.h"
#include "CItem.h"

#define ENTER 13

using namespace std;

void pressEnter(){ //Funcion para horientar al Jugador
	cout << "Preciona Enter para continuar\n";
	_getch();
}

void explorar() // Funcion 1 del Menu, donde sale: El combate, las tiendas, y otros posible
{
    CEnemy Enemigos; // La usamos para llamar la funcion "combate"
	int iNumero = 0; // Esta variable se usa para ver que evento saldra
	int iEnemigo = 0; // Esta variable se usa para ver que enemigo hara spawn (esto es temporal)
	int iNivel = 0; // Esta variable sirve para ver que nivel tendra el enemigo (esta tambien es temporal)
	srand(time(0)); //Esto es lo que hace que se randomize que eventos saldran

	iNumero = rand() % 10 + 1;  // se randomizan las variables
	iEnemigo = rand() % 3;
	iNivel = rand() % 20 + 1;
	
	if (iNumero >= 1 && iNumero <= 4) { // Evento 1: Combate con un enemigo random
       Enemigos.Combate(iEnemigo,iNivel);
       
	}
	else if (iNumero > 4 && iNumero <= 6) { // Evento 2: Situacion que Beneficia al Jugador 
		cout << "Buen Evento" << endl;
	}
	else if (iNumero > 6 && iNumero <= 8) { // Evento 3: Situacion que Perjudica al Jugador
		cout << "Mal Evento" << endl;
	}
	else if (iNumero > 8 && iNumero <= 9) { // Evento 4: Tienda "Secreta"
		cout << "Tienda Secreta" << endl;
	}
	else { // Literalmente no pasa nada .-.xD
		cout << "Nada ha pasado." << endl;
	}
}


int main(){
    CPlayer jugador;

	ofstream fileSaveOutput;
    ifstream fileSaveReader;
	
    // Esta variable es un Array para el menu
	string strMenu[7] = { "|1) Explorar                                            |","|2) Luchar contra el jefe final (Nivel Recomendado = 20)|","|3) Estatus                                             |","|4) Inventario/Vender                                   |","|5) Tienda                                              |","|6) Guardar Partida                                     |", "|7)Salir                                                |" };
	
    // sta variable es un Array para preguntarle al Usuario si quiere comenzar una nueva partida o Continuar su partida actual
    string strComienzo[2]= {"Empezar una nueva partida", "Continuar una partida"};
	string strNombreJugador; // Aqui se guarda el Nombre del Jugador
	
    int iPartida = 0; // Esta variable, es para la funcion de crear o continuar una Partina
    int iOpcion = 0; // Esta variable, es para la funcion del menu
    
    // Ambos chars tiene la misma funcion, Hacer que los Whiles de los menus, funcionen.
    char cFlecha = 'a';
    char cFlecha2 = 'a';
   
    while (cFlecha2 != ENTER) { // Su funcion es mantener el buche del menu de Comenzar partida
       
        system("cls");
        for (int i = 0; i < 2; i++) { // Este for, es para ver la flechita en el menu
        	
            if (iPartida == i) {
                cout << "> ";
            }
            else
            {
                cout << "  ";
            }
            
            cout << strComienzo[i] << endl; // El array del menu

        }
        switch (cFlecha2 = _getch()) { 
        case 's':
        	if (iPartida < 1){
            iPartida ++ ;
            
        }
        else if (iPartida == 3){ // Esto evita que la flecha de seleccion, se mueva mas abajo de lo debido
            iPartida = 2;

			}
		break;
        case 'w':
        	
        	if (iPartida > 0){
            iPartida --;
            break;        	
        }
    }
        
}
	switch(iPartida +1){ // Su funcion esta en el comienzo del Juego, es para poder crear una partida.
	case 1: // El caso de crear una nueva partida
		system("cls");
        cout << "Por favor introduce nombre tu nombre: ";
        cin >> strNombreJugador;

        fileSaveOutput.open("nombre.txt", ios::out | ios::trunc);

        if (fileSaveOutput.is_open())
        {
            fileSaveOutput << strNombreJugador;
            fileSaveOutput.close();
        }
    break;
    case 2: // El caso de no tener una partida en primer lugar, pero darle de todas formas a: "Continuar partida"
    {
    	system("cls");
        ifstream fileSaveReader("nombre.txt"); // El Archivo donde guardamos el Nombre del Jugador
        string sFileLine;
        int iCont;  
        
        if (!fileSaveReader){ // En caso de darle a continuar, pero no exite un archivo creado
        
        cout << "!Whoops, al parecer no hay un archivo! !Creemos uno!\n"; 
        pressEnter();
        
        cout << "Por favor introduce nombre tu nombre: ";
        cin >> strNombreJugador;

        fileSaveOutput.open("nombre.txt", ios::out | ios::trunc); // Grabamos el nombre del jugador y crea el archivo

        if (fileSaveOutput.is_open())
        {
            fileSaveOutput << strNombreJugador;
            fileSaveOutput.close();
        }
	}
       
        if (fileSaveReader.is_open())
        {
            iCont = 0;
            while (!fileSaveReader.eof())
            {
                getline(fileSaveReader, sFileLine);

                switch (iCont)
                {
                case 0: strNombreJugador = sFileLine;
                    break;
                }
                iCont++;
            }
            fileSaveReader.close();
            
        }
        break;
    }
}

    while (cFlecha != ENTER) { // Este while sirve par que el menu siga funcionando despues de escoger una opcion

        while (cFlecha != ENTER) { //Esto while hace que puedas navegar en el menu

            system("cls");
            cout << "Bienvenido "; cout << strNombreJugador; cout << ", Que quieres hacer?\n" << endl;
            for (int i = 0; i < 7; i++) { // For para la flecha

                if (iOpcion == i) {
                    cout << "> ";
                }
                else
                {
                    cout << "  ";
                }

                cout << strMenu[i] << endl; //aray del Menu

            }
            switch (cFlecha = _getch()) { // switch para escoger una opcion
            case 's':
                if (iOpcion < 6) {
                    iOpcion++;

                }
                else if (iOpcion == 7) {
                    iOpcion = 6;

                }
                break;
            case 'w':

                if (iOpcion > 0) {
                    iOpcion--;
                    break;
                }
            }

        }

        switch (iOpcion + 1) { // Lista de opciones del menu
        case 1:
            system("cls");

            explorar(); // Funcion de Exploracion (Aqui se encuentran, eventos, combates y tienda)

            pressEnter();
            cFlecha = 'a';
            break;
        case 2:
            system("cls");

            cout << "Luchar contra el jefe final (Nivel Recomendado: 20)"; // Un combate contra un jefe final.

            pressEnter();
            cFlecha = 'a';
            break;
        case 3:
            jugador.stats(); // Los stats del jugador

            pressEnter();
            cFlecha = 'a';
            break;
        case 4:
            system("cls");

            inventario(); //El inventario del jugador. Donde puede ver sus objetos, equiparlos o venderlos

            pressEnter();
            cFlecha = 'a';
            break;
        case 5:
            system("cls");

            cout << "Tienda" << endl; // Literalmente una tienda
            pressEnter();
            cFlecha = 'a';
            break;
        case 6:
            system("cls");
            cout << "Guadar Partida"; // Funcion de guardado de partida
            pressEnter();
            cFlecha = 'a';
            break;
        case 7:
            cout << "Se acabo el programa" << endl; // Fin del programa
        default:
            cout << "chao"; //Se imprime cuando decides salir del programa
        }
    }
	return 0;

}